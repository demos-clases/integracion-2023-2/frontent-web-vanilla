function mostrarDialogo(titulo, mensaje, fnOk) {
    document.getElementById('dialogo-titulo')
        .textContent = titulo;
    document.getElementById('dialogo-cuerpo')
        .textContent = mensaje;
    document.getElementById('dialogo-ok-btn').onclick = () => fnOk();
    let d = document.getElementById('contenedor-dialogo');
    d.style.display = 'block';
}

function ocultarDialogo() {
    let d = document.getElementById('contenedor-dialogo');
    d.style.display = 'none';
}