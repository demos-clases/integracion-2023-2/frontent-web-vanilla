let token = localStorage.getItem('loginToken');

if (token !== null) {
    document.getElementById('contentido-autorizado').style.display = 'block';
    document.getElementById('sin-acceso').style.display = 'none';
    obtenerUsuario();
} else {
    document.getElementById('sin-acceso').style.display = 'block';
    document.getElementById('contentido-autorizado').style.display = 'none';
}

function obtenerUsuario() {
    let token = localStorage.getItem('loginToken');
    fetch(
        'https://localhost:7253/api/personas',
        {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `bearer ${token}`,
            }
        }
    ).then( res => res.json())
    .then( data => console.log('DATOS:', data));
}