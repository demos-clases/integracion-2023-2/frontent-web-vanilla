function ingresar() {
    //GEt Form
    const fr = document.getElementById('form-login');
    // get form data
    const formData = new FormData(fr);
    // get values
    const loginData = Object.fromEntries(formData);
    console.log('>>>', loginData);

    fetch(
        'https://localhost:7253/api/seguridad/login',
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(loginData),
        }
    ).then(
        response => {
            if (response.status >= 200 && response.status < 300  ) {
                return response.json()
            } else if (response.status == 401) {
                mostrarDialogo('Error al ingresar',
                    'Usuario o contraseña invalida',
                    () => {
                        ocultarDialogo();
                    }
                )
            } else {
                mostrarDialogo('Error al ingresar',
                    'Lo sentimos.... estamos trabajando',
                    () => {
                        ocultarDialogo();
                    }
                )
            }
            
        }
    ).then(
        data => ingresoCorrecto(data),
    ).catch(
        error => {
            mostrarDialogo('Error al ingresar',
            'Lo sentimos.... estamos trabajando',
            () => {
                ocultarDialogo();
            }
            )
        }
    );
}

function ingresoCorrecto(datosIngreso) {
    localStorage.setItem('loginToken', datosIngreso.token);
    localStorage.setItem('usuarioLogeado', JSON.stringify(datosIngreso.usuario));
    mostrarDialogo('Ingreso correcto',
        `Bienvenido de nuevo ${datosIngreso.usuario.nombres}`,
        () => {
            window.location.href = '/protegida.html';
        }
    )
    console.log(datosIngreso);
}