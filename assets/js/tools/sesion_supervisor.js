// Ejecutamos una rutina cada n segundos (30)
setInterval(function(){
    // buscamos el token en el localstorage
    let token = localStorage.getItem('loginToken');

    if (token !== null) { 
        // si el token existe, comprobamos que sea valido
        const b64 = token.split('.')[1]; // sacamos el payload
        const jsonStr = atob(b64); // lo pasamos de b64 a ascii
        const jwt = JSON.parse(jsonStr); // lo convertimos a objeto de JS
        const exp = new Date(jwt.exp * 1000); // obtenemos la fecha de expiracion desde el formato unix
        const now = new Date(); // obtenemos la fecha y hora actual

        if (exp.getTime() <= now.getTime()) { // comparamos que la expiracion sea menor a la fecha actual
            // si es menor cerramos la sesion
            console.log('SESION CADUCADA');
            cerrarSesion();
        } else {
            window.minutosHastaCerraSesion = minutosEntreDosFechas(exp, now);
            document.getElementById('minutosRestantes').innerText = window.minutosHastaCerraSesion;
            // si es mayor no hacemos nada, porque la sesion esta activa
            if (window.minutosHastaCerraSesion <= 8 ) {
                const conf = confirm("La sesion esta por cerrar. Desea continuar?");
                if (conf) {
                    refreshSesion();
                }
            }
            console.log('SESION ACTIVA');
        }
        console.log(jwt);

    } else {
        // CERRAR SESION
        cerrarSesion();
    }

}, 10000);

function cerrarSesion() {
    localStorage.clear();
    window.location.href = '/login.html';
}

function minutosEntreDosFechas(fecha1, fecha2) {
    const diff = Math.abs(fecha1 - fecha2);
    const minutes = Math.floor((diff/1000)/60);
    return minutes;
}

function refreshSesion() {
    let token = localStorage.getItem('loginToken');
    fetch(
        'https://localhost:7253/api/seguridad/refresh-token',
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: '{}',
        }
    ).then(
        response => {
            if (response.status >= 200 && response.status < 300  ) {
                return response.json()
            }
        }
    ).then(
        data => {
            localStorage.setItem('loginToken', data.token)
        },
    ).catch(
        error => {
            mostrarDialogo('Error al ingresar',
            'Lo sentimos.... estamos trabajando',
            () => {
                ocultarDialogo();
            }
            )
        }
    );
}