

function registrarUsuario() {
    //GEt Form
    const fr = document.getElementById('form-registro');
    // get form data
    const formData = new FormData(fr);
    // get values
    const nuevoUsuario = Object.fromEntries(formData);
    console.log('>>>', nuevoUsuario);

    fetch(
        'https://localhost:7253/api/usuarios',
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(nuevoUsuario),
        }
    ).then(
        response => response.json()
    ).then(
        data => usuarioCreado(data),
    ).catch(
        error => {
            mostrarDialogo('Error al registrar',
            'Lo sentimos.... estamos trabajando',
            () => {
                ocultarDialogo();
            }
            )
        }
    );
}

function usuarioCreado(usuario) {
    mostrarDialogo('Registro correcto', 
        `Se a creado correctamente el usuario ${usuario.nombres}`,
        () => {
            window.location.href = '/login.html';
        }
    );
}


